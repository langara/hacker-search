package pl.mareklangiewicz.hackersearch

import android.support.test.runner.AndroidJUnit4
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import pl.mareklangiewicz.myloggers.MY_DEFAULT_ANDRO_LOGGER
import pl.mareklangiewicz.myutils.lsubscribe
import pl.mareklangiewicz.myutils.str
import pl.mareklangiewicz.myutils.w


@RunWith(AndroidJUnit4::class)
class GitHubCacheTests {

    val log = MY_DEFAULT_ANDRO_LOGGER

    // tests below are not really proper unit tests. just some simple code for manual testing/logging/debugging

    private val service = GitHubCache
//    private val service = GitHub // it is possible to switch service here, and launch some tests carefully one by one (and check logcat).
//    private val service = Model  // it is possible to switch service here, and launch some tests carefully one by one (and check logcat).



    @Ignore
    @Test fun clear() = service.clear()



    @Ignore
    @Test fun putLangaraDetails() {
        GitHub.getUserDetails("langara").lsubscribe(log) {
            log.w("Writing user details: ${it.str}")
            service.putUserDetails(it as IUserDetails)
        }
    }

    @Ignore
    @Test fun putLangaraDetailsModified() {
        GitHub.getUserDetails("langara").lsubscribe(log) {
            val data = (it as GitHub.UserDetails).copy(email = "bla@ble.com")
            log.w("Writing user details: $data")
            service.putUserDetails(data)
        }
    }

    @Ignore
    @Test fun getLangaraDetails() {
        service.getUserDetails("langara").lsubscribe(log) {
            log.w("User details from realm: ${it.str}")
            log.w("User details from realm - email: ${it.email}")
            log.w("User details from realm - time : ${"%tT".format(it.time_stamp)}")
        }
    }




    @Ignore
    @Test fun putAbiLogDetails() {
        GitHub.getRepoDetails("langara", "AbiLog").lsubscribe(log) {
            log.w("Writing repo details: ${it.str}")
            service.putRepoDetails(it as IRepoDetails)
        }
    }

    @Ignore
    @Test fun putAbiLogDetailsModified() {
        GitHub.getRepoDetails("langara", "AbiLog").lsubscribe(log) {
            val data = (it as GitHub.RepoDetails).copy(language = "Haskellllll")
            log.w("Writing repo details: ${data.str}")
            service.putRepoDetails(data)
        }
    }

    @Ignore
    @Test fun getAbiLogDetails() {
        service.getRepoDetails("langara", "AbiLog").lsubscribe(log) {
            log.w("Repo details from realm: ${it.str}")
            log.w("Repo details from realm - language: ${it.language}")
            log.w("Repo details from realm - time    : ${"%tT".format(it.time_stamp)}")
        }
    }



    private fun putSomeUsers(query: String) {
        GitHub.getUsers(query).lsubscribe(log) {
            log.w("Writing ${(it as IUsers).items.size} users for query: \"$query\": ${it.str}")
            service.putUsers(query, it)
        }
    }

    private fun getSomeUsers(query: String) {
        service.getUsers(query).lsubscribe(log) {
            log.w("Users for query: \"$query\": ${it.str}")
            log.w("Users for query: \"$query\" - total_count: ${it.total_count}")
        }
    }


    @Ignore
    @Test fun putLangaraUsers() = putSomeUsers("langara")

    @Ignore
    @Test fun putLangUsers() = putSomeUsers("lang")


    @Ignore
    @Test fun getLangaraUsers() = getSomeUsers("langara")

    @Ignore
    @Test fun getLangUsers() = getSomeUsers("lang")




    private fun putSomeRepos(query: String) {
        GitHub.getRepos(query).lsubscribe(log) {
            log.w("Writing ${(it as IRepos).items.size} repos for query: \"$query\": ${it.str}")
            service.putRepos(query, it)
        }
    }

    private fun getSomeRepos(query: String) {
        service.getRepos(query).lsubscribe(log) {
            log.w("Repos for query: \"$query\": ${it.str}")
            log.w("Repos for query: \"$query\" - total_count: ${it.total_count}")
        }
    }


    @Ignore
    @Test fun putRxJavaRepos() = putSomeRepos("RxJava")

    @Ignore
    @Test fun putRxRepos() = putSomeRepos("Rx")


    @Ignore
    @Test fun getRxJavaRepos() = getSomeRepos("RxJava")

    @Ignore
    @Test fun getRxRepos() = getSomeRepos("Rx")

    private fun putSomeItems(query: String) {
        GitHub.getItems(query).lsubscribe(log) {
            log.w("Writing ${(it as IItems).items.size} items for query: \"$query\": ${it.str}")
            service.putItems(query, it)
        }
    }

    private fun getSomeItems(query: String) {
        service.getItems(query).lsubscribe(log) {
            log.w("Items for query: \"$query\": ${it.str}")
            log.w("Items for query: \"$query\" - total_count: ${it.total_count}")
        }
    }

    @Ignore
    @Test fun putHelloItems() = putSomeItems("Hello")

    @Ignore
    @Test fun putHellItems() = putSomeItems("Hell")


//    @Ignore
    @Test fun getHelloItems() = getSomeItems("Hello")

//    @Ignore
    @Test fun getHellItems() = getSomeItems("Hell")

}

