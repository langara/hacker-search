package pl.mareklangiewicz.hackersearch

import android.widget.TextView

/** Created by Marek Langiewicz on 20.06.16. */
open class ViewText(private val tview: TextView) : IViewText {
    override var text: String
        get() = tview.text.toString()
        set(value) {tview.text = value}
}