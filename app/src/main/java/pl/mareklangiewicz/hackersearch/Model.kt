package pl.mareklangiewicz.hackersearch

import android.support.annotation.MainThread
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Marek Langiewicz on 16.06.16.
 * App "business logic" and facade for data access.
 * It should be used from MainThread only.
 * It manages changing threads back and forth when doing IO.
 */
@MainThread
object Model : IModel {

    private val network = GitHub
    private val cache = GitHubCache

    private val USE_NETWORK = true
    private val USE_CACHE = true
    private val UPDATE_CACHE = true


    override fun getItems(query: String): Observable<out IItems> {

        val cacheObservable = if(USE_CACHE) cache.getItems(query) else Observable.empty()

        var networkObservable = if(USE_NETWORK) network.getItems(query) else Observable.empty()

        if(UPDATE_CACHE)
            networkObservable = networkObservable.doOnNext { putItems(query, it as IItems) }

        return Observable.concat(cacheObservable, networkObservable)
                .take(1) // it should subscribe to networkObservable only if cacheObservable is empty.
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    // Like getItems, but for users only. This method is not really needed now. Left for tests/experiments
    override fun getUsers(query: String): Observable<out IUsers> {

        val cacheObservable = if(USE_CACHE) cache.getUsers(query) else Observable.empty()

        var networkObservable = if(USE_NETWORK) network.getUsers(query) else Observable.empty()

        if(UPDATE_CACHE)
            networkObservable = networkObservable.doOnNext { putUsers(query, it as IUsers) }

        return Observable.concat(cacheObservable, networkObservable)
                .take(1) // it should subscribe to networkObservable only if cacheObservable is empty.
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    // Like getItems, but for repos only. This method is not really needed now. Left for tests/experiments
    override fun getRepos(query: String): Observable<out IRepos> {

        val cacheObservable = if(USE_CACHE) cache.getRepos(query) else Observable.empty()

        var networkObservable = if(USE_NETWORK) network.getRepos(query) else Observable.empty()

        if(UPDATE_CACHE)
            networkObservable = networkObservable.doOnNext { putRepos(query, it as IRepos) }

        return Observable.concat(cacheObservable, networkObservable)
                .take(1) // it should subscribe to networkObservable only if cacheObservable is empty.
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    // The same logic as in getItems
    override fun getUserDetails(login: String): Observable<out IUserDetails> {

        val cacheObservable = if(USE_CACHE) cache.getUserDetails(login) else Observable.empty()

        var networkObservable = if(USE_NETWORK) network.getUserDetails(login) else Observable.empty()

        if(UPDATE_CACHE)
            networkObservable = networkObservable.doOnNext { putUserDetails(it as IUserDetails) }


        return Observable.concat(cacheObservable, networkObservable)
                .take(1) // it should subscribe to networkObservable only if cacheObservable is empty.
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    // The same logic as in getItems
    override fun getRepoDetails(owner: String, repo: String): Observable<out IRepoDetails> {

        val cacheObservable = if(USE_CACHE) cache.getRepoDetails(owner, repo) else Observable.empty()

        var networkObservable = if(USE_NETWORK) network.getRepoDetails(owner, repo) else Observable.empty()

        if(UPDATE_CACHE)
            networkObservable = networkObservable.doOnNext { putRepoDetails(it as IRepoDetails) }

        return Observable.concat(cacheObservable, networkObservable)
                .take(1) // it should subscribe to networkObservable only if cacheObservable is empty.
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }


    override fun putItems(query: String, items: IItems) = cache.putItems(query, items)
    override fun putUsers(query: String, users: IUsers) = cache.putUsers(query, users)
    override fun putRepos(query: String, repos: IRepos) = cache.putRepos(query, repos)
    override fun putUserDetails(user: IUserDetails) = cache.putUserDetails(user)
    override fun putRepoDetails(repo: IRepoDetails) = cache.putRepoDetails(repo)

    /** Clears all cached data. */
    override fun clear() = cache.clear()
}
