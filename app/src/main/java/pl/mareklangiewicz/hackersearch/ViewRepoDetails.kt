package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.RecyclerView
import android.widget.*

/** Created by Marek Langiewicz on 21.06.16. */
class ViewRepoDetails(
        override val input: IViewInput,
        override val notes: IViewNotes,
        override val progress: IViewProgress,
        override val status: IViewText
) : IViewRepoDetails {
    constructor(
            etinput: EditText,
            rvnotes: RecyclerView,
            pbprogress: ProgressBar,
            tvstatus: TextView
    ) : this(
            ViewInput(etinput),
            ViewNotes(rvnotes),
            ViewProgress(pbprogress),
            ViewText(tvstatus)
    )
}
