package pl.mareklangiewicz.hackersearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.hs_fragment_repo_details.*
import pl.mareklangiewicz.myfragments.MyFragment

@Suppress("unused")
class HSFragmentRepoDetails : MyFragment() {

    private val presenter = PresenterRepoDetails()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manager?.name = getString(R.string.hs_repo_details)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.hs_fragment_repo_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.view = ViewRepoDetails(
                hs_rdetails_input,
                hs_rdetails_notes,
                hs_rdetails_progress,
                hs_rdetails_status
        )
        presenter.view!!.input.text = (activity as HSActivity).query
    }

    override fun onDestroyView() {
        (activity as HSActivity).query = ""
        presenter.view = null
        super.onDestroyView()
    }
}