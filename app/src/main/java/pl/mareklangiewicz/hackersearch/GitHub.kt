package pl.mareklangiewicz.hackersearch

import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.ArrayList

/**
 * A service for accessing GitHub data (using Retrofit 2 library)
 * IMPORTANT: GitHub has some rate limits. Details:
 * https://developer.github.com/v3/#rate-limiting
 * Usually it returns 403 after too many requests,
 * and we just have to wait a minute.
 */
object GitHub : IServiceItems, IServiceUsers, IServiceRepos, IClear {

    const val URL = "https://api.github.com"

    const val ACCEPT = "Accept: application/vnd.github.v3+json"
    const val AGENT = "User-Agent: HackerAgent"

    data class User(
            override val login: String,
            override val id: Long,
//            override val avatar_url: String?,
//            val url: String?,
            override val html_url: String?
    ) : IUser

    data class UserDetails(
            override val login: String,
            override val id: Long,
            override val avatar_url: String?,
//            val url: String?,
            override val html_url: String?,
            override val name: String?,
            override val company: String?,
//            val blog: String?,
            override val location: String?,
            override val email: String?,
//            val hireable: Boolean?,
//            val bio: String?,
            override val public_repos: Long?,
            override val public_gists: Long?,
            override val followers: Long?,
            override val following: Long?
//            val created_at: String?,
//            val updated_at: String?
    ) : IUserDetails

    data class Repo(
            override val id: Long,
            override val name: String,
            override val full_name: String?,
            override val html_url: String?,
            override val language: String?
    ) : IRepo

    data class RepoDetails(
            override val id: Long,
            override val name: String,
            override val full_name: String?,
            override val description: String?,
            override val html_url: String?,
            override val homepage: String?,
            override val language: String?,
            override val forks_count: Long?,
            override val stargazers_count: Long?,
            override val watchers_count: Long?
//            val default_branch: String?,
//            val open_issues_count: Long?,
//            val pushed_at: String?,
//            val created_at: String?,
//            val updated_at: String?
    ) : IRepoDetails

    data class Users(
            override val total_count: Long,
            override val incomplete_results: Boolean,
            override val items: List<User>
    ) : IUsers

    data class Repos(
            override val total_count: Long,
            override val incomplete_results: Boolean,
            override val items: List<Repo>
    ) : IRepos

    data class Items(
            override val total_count: Long,
            override val incomplete_results: Boolean,
            override val items: List<IItem>
    ) : IItems

    private interface Service {

        @Headers(ACCEPT, AGENT)
        @GET("/users/{login}")
        fun getUserDetails(@Path("login") login: String): Observable<UserDetails>

        @Headers(ACCEPT, AGENT)
        @GET("/repos/{owner}/{repo}")
        fun getRepoDetails(@Path("owner") owner: String, @Path("repo") repo: String): Observable<RepoDetails>

        @Headers(ACCEPT, AGENT)
        @GET("/search/repositories?per_page=40")
        fun getRepos(@Query("q") query: String): Observable<Repos>

        @Headers(ACCEPT, AGENT)
        @GET("/search/users?per_page=40")
        fun getUsers(@Query("q") query: String): Observable<Users>
    }

    private val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    private val service = retrofit.create(Service::class.java)

    private fun join(users: IUsers, repos: IRepos): Items = Items(
            users.total_count + repos.total_count,
            users.incomplete_results || repos.incomplete_results,
            ArrayList<IItem>(users.items.size + repos.items.size).apply { addAll(users.items); addAll(repos.items); sortBy { it.id } }
    )

    override fun getItems(query: String): Observable<out IItems>
            = Observables.zip(getUsers(query),getRepos(query)) { users, repos -> join(users, repos) }

    override fun putItems(query: String, items: IItems) = throw UnsupportedOperationException()

    override fun getUsers(query: String): Observable<out IUsers> = service.getUsers(query)
    override fun getRepos(query: String): Observable<out IRepos> = service.getRepos(query)

    override fun putUsers(query: String, users: IUsers) = throw UnsupportedOperationException()
    override fun putRepos(query: String, repos: IRepos) = throw UnsupportedOperationException()

    override fun getUserDetails(login: String): Observable<out IUserDetails> = service.getUserDetails(login)
    override fun getRepoDetails(owner: String, repo: String): Observable<out IRepoDetails> = service.getRepoDetails(owner, repo)

    override fun putUserDetails(user: IUserDetails) = throw UnsupportedOperationException()
    override fun putRepoDetails(repo: IRepoDetails) = throw UnsupportedOperationException()

    /** Formats all github servers. Not really :-) */
    override fun clear() = throw UnsupportedOperationException()
}
