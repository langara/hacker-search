package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.CardView
import kotlinx.android.synthetic.main.hs_note.view.*

/** Created by Marek Langiewicz on 21.06.16. */
class ViewNote(private val cview: CardView) : IViewNote {

    override var note: Note? = null
        set(value) {
            field = value
            cview.hs_note_label.text = value?.label ?: ""
            cview.hs_note_content.text = value?.content ?: ""
        }
}