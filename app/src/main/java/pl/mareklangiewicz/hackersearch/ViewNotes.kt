package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.*
import android.view.*

/** Created by Marek Langiewicz on 21.06.16. */
class ViewNotes(private val recycler: RecyclerView) : IViewNotes {

    private val adapter = Adapter()

    init { recycler.adapter = adapter }

    override var notes: List<Note>? = null
        set(value) {
            field = value
            adapter.notifyDataSetChanged()
        }

    inner class Holder(cview: CardView) : RecyclerView.ViewHolder(cview) {
        val vnote: ViewNote = ViewNote(cview)
    }

    inner class Adapter() : RecyclerView.Adapter<Holder>() {

        override fun getItemCount() = notes?.size ?: 0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.hs_note, parent, false)
            return Holder(v as CardView)
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.vnote.note = notes?.get(position)
        }
    }
}
