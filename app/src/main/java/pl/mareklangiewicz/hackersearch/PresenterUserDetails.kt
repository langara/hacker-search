package pl.mareklangiewicz.hackersearch

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import pl.mareklangiewicz.myloggers.MY_DEFAULT_ANDRO_LOGGER
import pl.mareklangiewicz.myutils.lsubscribe
import java.util.concurrent.TimeUnit


/** Created by Marek Langiewicz on 20.06.16. */
class PresenterUserDetails : IPresenter<ViewUserDetails> {

    var delay = 500L
    var minLength = 3

    private val log = MY_DEFAULT_ANDRO_LOGGER

    private val model = Model

    private var subInput: Disposable? = null
    private var subDetails: Disposable? = null

    override var view: ViewUserDetails? = null

        set(v) {

            field = v

            subInput?.dispose()

            if(v === null)
                return

            working = working // sync ui..

            subInput = v.input.changes
                    .debounce(delay, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .filter { it.length >= minLength }
                    .lsubscribe(log) {
                        getUserDetails(it)
                    }
        }

    private var working: Boolean = false // true if we are running long operation (displays some moving progress bar)
        set(value) {
            field = value
            val v = view ?: return
            v.progress.indeterminate = value
        }

    private fun getUserDetails(login: String) {
        subDetails?.dispose()
        subDetails = model.getUserDetails(login)
                .doOnSubscribe { working = true }
                .doOnTerminate { working = false }
                .lsubscribe(log, logOnError = "[SNACK]Error: %s") {
                    showUserDetails(it as IUserDetails)
                }
    }

    private fun showUserDetails(details: IUserDetails?) {
        val v = view ?: return
        v.avatar.url = details?.avatar_url ?: ""
        v.name.text = details?.name ?: ""
        v.description.text = "%s\n%s\n%s".format(details?.location ?: "", details?.email ?: "", details?.company ?: "")
        v.notes.notes = listOf(
                Note("Id", details?.id?.toString() ?: ""),
                Note("Login", details?.login ?: ""),
                Note("GitHub page", details?.html_url ?: ""),
                Note("E-Mail", details?.email ?: ""),
                Note("Name", details?.name ?: ""),
                Note("Company", details?.company ?: ""),
                Note("Location", details?.location ?: ""),
                Note("Public repos", details?.public_repos?.toString() ?: ""),
                Note("Public gists", details?.public_gists?.toString() ?: ""),
                Note("Followers", details?.followers?.toString() ?: ""),
                Note("Following", details?.following?.toString() ?: "")
        )
    }
}
