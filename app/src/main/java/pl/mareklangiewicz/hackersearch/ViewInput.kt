package pl.mareklangiewicz.hackersearch

import android.widget.EditText
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable

/** Created by Marek Langiewicz on 20.06.16. */
class ViewInput(private val etext: EditText) : ViewText(etext), IViewInput {
    override val changes: Observable<String> = etext.textChanges().map { it.toString() }
}
