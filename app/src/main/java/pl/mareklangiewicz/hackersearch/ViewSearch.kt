package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.RecyclerView
import android.widget.*

/** Created by Marek Langiewicz on 20.06.16. */
class ViewSearch(private val activity: HSActivity,
        override val input: IViewInput,
        override val results: IViewItems,
        override val progress: IViewProgress,
        override val status: IViewText
) : IViewSearch {
    constructor(activity: HSActivity,
                etinput: EditText,
                rvresults: RecyclerView,
                pbprogress: ProgressBar,
                tvstatus: TextView
    )
    : this(activity,
            ViewInput(etinput),
            ViewItems(rvresults),
            ViewProgress(pbprogress),
            ViewText(tvstatus)
    )
    fun switchToUserDetails() { activity.execute("fragment .HSFragmentUserDetails") }
    fun switchToRepoDetails() { activity.execute("fragment .HSFragmentRepoDetails") }
}