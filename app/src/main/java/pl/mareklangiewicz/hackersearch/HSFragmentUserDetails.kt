package pl.mareklangiewicz.hackersearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.hs_basic_user_info.*
import kotlinx.android.synthetic.main.hs_fragment_user_details.*
import pl.mareklangiewicz.myfragments.MyFragment

@Suppress("unused")
class HSFragmentUserDetails : MyFragment() {

    private val presenter = PresenterUserDetails()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manager?.name = getString(R.string.hs_user_details)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.hs_fragment_user_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.view = ViewUserDetails(
                hs_udetails_input,
                hs_avatar,
                hs_user_name,
                hs_user_description,
                hs_udetails_notes,
                hs_udetails_progress,
                hs_udetails_status
        )
        presenter.view!!.input.text = (activity as HSActivity).query
    }

    override fun onDestroyView() {
        (activity as HSActivity).query = ""
        presenter.view = null
        super.onDestroyView()
    }
}