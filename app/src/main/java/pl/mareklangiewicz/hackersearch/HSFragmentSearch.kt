package pl.mareklangiewicz.hackersearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.hs_fragment_search.*
import pl.mareklangiewicz.myfragments.MyFragment

@Suppress("unused")
class HSFragmentSearch : MyFragment() {

    private val presenter = PresenterSearch()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manager?.name = getString(R.string.hs_search)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.hs_fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.view = ViewSearch(activity as HSActivity, hs_search_input, hs_search_results, hs_search_progress, hs_search_status)
        presenter.view!!.input.text = (activity as HSActivity).query
    }

    override fun onDestroyView() {
        (activity as HSActivity).query = presenter.view!!.input.text
        presenter.view = null
        super.onDestroyView()
    }
}