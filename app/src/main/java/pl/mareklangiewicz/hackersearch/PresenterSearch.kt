package pl.mareklangiewicz.hackersearch

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import pl.mareklangiewicz.myloggers.MY_DEFAULT_ANDRO_LOGGER
import pl.mareklangiewicz.myutils.lsubscribe
import java.util.concurrent.TimeUnit

/** Created by Marek Langiewicz on 20.06.16. */
class PresenterSearch : IPresenter<ViewSearch> {

    var delay = 500L
    var minLength = 3

    private val log = MY_DEFAULT_ANDRO_LOGGER

    private val model = Model

    private var subInput: Disposable? = null
    private var subClick: Disposable? = null
    private var subItems: Disposable? = null

    override var view: ViewSearch? = null

        set(v) {

            field = v

            subInput?.dispose()
            subClick?.dispose()

            if(v === null)
                return

            working = working // sync ui..

            subInput = v.input.changes
                    .debounce(delay, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .filter { it.length >= minLength }
                    .lsubscribe(log) {
                        getItems(it)
                    }

            subClick = v.results.clicks.lsubscribe(log) { item ->
                when(item.type) {
                    "user" -> view?.run { input.text = item.title; switchToUserDetails() }
                    "repo" -> view?.run { input.text = item.title; switchToRepoDetails() }
                }
            }
        }

    private var working: Boolean = false // true if we are running long operation (displays some moving progress bar)
        set(value) {
            field = value
            val v = view ?: return
            v.progress.indeterminate = value
        }


    private fun getItems(query: String) {
        subItems?.dispose()
        subItems = model.getItems(query)
                .doOnSubscribe { working = true }
                .doOnTerminate { working = false }
                .lsubscribe(log, logOnError = "[SNACK]Error: %s") {
                    view?.run {
                        results.items = it as IItems
                    }
                }
    }
}
