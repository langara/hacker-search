package pl.mareklangiewicz.hackersearch

import android.os.Bundle
import android.view.View
import hu.supercluster.paperwork.Paperwork
import pl.mareklangiewicz.myfragments.MyAboutFragment
import pl.mareklangiewicz.myutils.str
import java.util.*

@Suppress("unused")
class HSFragmentAbout : MyAboutFragment() {

    private fun Long.asTimeStr() = "%tF %tT".format(Locale.getDefault(), this, this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        manager?.name = getString(R.string.hs_about)

        val paperwork = Paperwork(activity)

        title = getString(R.string.hs_app_name)
        description = "This app allows to search GitHub for users and repositories. " +
                "It starts to search for any entered user/repo name automatically after 0.5sec (if it has at least 3 letters). " +
                "You can click on user/repo id to jump to the details screen automatically. " +
                "IMPORTANT: GitHub has some limits of requests per minute, so sometimes you can get error: 403. " +
                "Then you just have to wait a minute. This app caches all data locally, so all already downloaded data " +
                "should be available anyway."

        details = listOf(
                "build type" to BuildConfig.BUILD_TYPE,
                "build flavor" to BuildConfig.FLAVOR,
                "version code" to BuildConfig.VERSION_CODE.str,
                "version name" to BuildConfig.VERSION_NAME,
                "build time" to paperwork.get("buildTime"),
                "app start time" to HSApplication.START_TIME.asTimeStr(),
                "git sha" to paperwork.get("gitSha"),
                "git tag" to paperwork.get("gitTag"),
                "git info" to paperwork.get("gitInfo")
        )
    }
}



