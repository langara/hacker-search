package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jakewharton.rxrelay2.PublishRelay

/** Created by Marek Langiewicz on 20.06.16. */
class ViewItems(private val recycler: RecyclerView) : IViewItems {

    private val adapter = Adapter()

    init { recycler.adapter = adapter }

    override var items: IItems? = null
        set(value) {
            field = value
            adapter.notifyDataSetChanged()
        }

    override val clicks: PublishRelay<IItem> = PublishRelay.create()

    inner class Holder(cview: CardView) : RecyclerView.ViewHolder(cview) {
        val vitem: ViewItem = ViewItem(cview)
        init {
            cview.setOnClickListener { vitem.item?.let { clicks.accept(it) } }
        }
    }


    inner class Adapter() : RecyclerView.Adapter<Holder>() {

        init { setHasStableIds(true) }

        override fun getItemCount() = items?.items?.size ?: 0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.hs_item, parent, false)
            return Holder(v as CardView)
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.vitem.item = items!!.items[position]
        }

        override fun getItemId(position: Int) = items!!.items[position].id
    }

}
