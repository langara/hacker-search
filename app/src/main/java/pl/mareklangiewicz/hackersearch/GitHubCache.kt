package pl.mareklangiewicz.hackersearch

import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmQuery
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

/**
 * Created by Marek Langiewicz on 16.06.16.
 * GitHub cache implementation with Realm database
 *
 * All this data classes below should be nested in GitHubCache,
 * but Realm doesn't allow that yet..
 */



const val CACHE_TIME = 10 * 60 * 60 * 1000 // we keep cached data for 10 hours


interface ITimeStamp {
    val time_stamp: Long
    fun isUpToDate() = time_stamp > System.currentTimeMillis() - CACHE_TIME
}


open class RealmItem(
        @PrimaryKey override var id: Long = 0,
        @Required @Index override var title: String = "",
        @Required override var type: String = "",
        override var html_url: String? = null,
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), IItem, ITimeStamp {
    constructor(ii: IItem) : this(ii.id, ii.title, ii.type, ii.html_url)
}



open class RealmItems(
        @Required @PrimaryKey var query: String = "",
        var total_count: Long = 0,
        var incomplete_results: Boolean = false,
        var items: RealmList<RealmItem> = RealmList(),
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), ITimeStamp {
    constructor(query: String, iis: IItems) : this(query, iis.total_count, iis.incomplete_results) {
        for(ii in iis.items)
            items.add(RealmItem(ii))
    }

    // This is workaround. I should just make RealmItems class implement IItems,
    // (and add "out" to "items" type so it would be: RealmList<out RealmItem>)
    // but then I get strange compiler error... NullPointerException :-)
    // (it looks like a bug in Realm plugin magic...)
    fun asIItems() = object : IItems {
        override val items: List<IItem> get() = this@RealmItems.items
        override val total_count: Long get() = this@RealmItems.total_count
        override val incomplete_results: Boolean get() = this@RealmItems.incomplete_results
    }
}



// Not really needed (I use RealmItem instead). Left for tests/experiments
open class RealmUser(
        @PrimaryKey override var id: Long = 0,
        @Required @Index override var login: String = "",
        override var html_url: String? = null,
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), IUser, ITimeStamp {
    constructor(iu: IUser) : this(iu.id, iu.login, iu.html_url)
}




open class RealmUserDetails(
        @PrimaryKey override var id: Long = 0,
        @Required @Index override var login: String = "",
        override var avatar_url: String? = null,
        override var html_url: String? = null,
        override var name: String? = null,
        override var company: String? = null,
        override var location: String? = null,
        override var email: String? = null,
        override var public_repos: Long? = null,
        override var public_gists: Long? = null,
        override var followers: Long? = null,
        override var following: Long? = null,
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), IUserDetails, ITimeStamp {
    constructor(iud: IUserDetails) : this(iud.id, iud.login, iud.avatar_url, iud.html_url, iud.name, iud.company, iud.location,
            iud.email, iud.public_repos, iud.public_gists, iud.followers, iud.following)
}




// Not really needed (I use RealmItem instead). Left for tests/experiments
open class RealmRepo(
        @PrimaryKey override var id: Long = 0,
        @Required @Index override var name: String = "",
        override var full_name: String? = null,
        override var html_url: String? = null,
        override var language: String? = null,
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), IRepo, ITimeStamp {
    constructor(ir: IRepo) : this(ir.id, ir.name, ir.full_name, ir.html_url, ir.language)
}





open class RealmRepoDetails(
        @PrimaryKey override var id: Long = 0,
        @Required @Index override var name: String = "",
        override var full_name: String? = null,
        override var description: String? = null,
        override var html_url: String? = null,
        override var homepage: String? = null,
        override var language: String? = null,
        override var forks_count: Long? = null,
        override var stargazers_count: Long? = null,
        override var watchers_count: Long? = null,
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), IRepoDetails, ITimeStamp {
    constructor(ird: IRepoDetails) : this(ird.id, ird.name, ird.full_name, ird.description, ird.html_url,
            ird.homepage, ird.language, ird.forks_count, ird.stargazers_count, ird.watchers_count)
}




// Not really needed (I use RealmItems instead). Left for tests/experiments
open class RealmUsers(
        @Required @PrimaryKey var query: String = "",
        var total_count: Long = 0,
        var incomplete_results: Boolean = false,
        var items: RealmList<RealmUser> = RealmList(),
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), ITimeStamp {
    constructor(query: String, ius: IUsers) : this(query, ius.total_count, ius.incomplete_results) {
        for(iu in ius.items)
            items.add(RealmUser(iu))
    }

    // see comment for: fun RealmItems.asIItems
    fun asIUsers() = object : IUsers {
        override val items: List<IUser> get() = this@RealmUsers.items
        override val total_count: Long get() = this@RealmUsers.total_count
        override val incomplete_results: Boolean get() = this@RealmUsers.incomplete_results
    }
}





// Not really needed now. (I use RealmItems instead). Left for tests/experiments
open class RealmRepos(
        @Required @PrimaryKey var query: String = "",
        var total_count: Long = 0,
        var incomplete_results: Boolean = false,
        var items: RealmList<RealmRepo> = RealmList(),
        override var time_stamp: Long = System.currentTimeMillis()
) : RealmObject(), ITimeStamp {
    constructor(query: String, irs: IRepos) : this(query, irs.total_count, irs.incomplete_results) {
        for(ir in irs.items)
            items.add(RealmRepo(ir))
    }

    // see comment for: fun RealmItems.asIItems
    fun asIRepos() = object : IRepos {
        override val items: List<IRepo> get() = this@RealmRepos.items
        override val total_count: Long get() = this@RealmRepos.total_count
        override val incomplete_results: Boolean get() = this@RealmRepos.incomplete_results
    }
}



object GitHubCache : IServiceItems, IServiceUsers, IServiceRepos, IClear {

    private fun transaction(block: Realm.() -> Unit) {
        Realm.getDefaultInstance().use {
            it.executeTransaction {
                it.block()
            }
        }
    }

    /** Clears all cached data. */
    override fun clear() = transaction { deleteAll() }

    private fun put(obj: RealmObject) = transaction { copyToRealmOrUpdate(obj) }

    override fun putItems(query: String, items: IItems) { put(RealmItems(query, items)) }
    override fun putUsers(query: String, users: IUsers) { put(RealmUsers(query, users)) } // Not really needed. I only use putItems now.
    override fun putRepos(query: String, repos: IRepos) { put(RealmRepos(query, repos)) } // Not really needed. I only use putItems now.
    override fun putUserDetails(user: IUserDetails)     { put(RealmUserDetails(user)) }
    override fun putRepoDetails(repo: IRepoDetails)     { put(RealmRepoDetails(repo)) }


    private fun <T> T.asObservable(): Observable<T> =
        if (this !== null) Observable.just(this) else Observable.empty()

    private inline fun <reified RO : RealmObject>get(crossinline filter: RealmQuery<RO>.() -> RealmQuery<RO>): Observable<RO> {
        return Observable.defer {
            Realm.getDefaultInstance().use { realm ->
                realm.where(RO::class.java).filter().findFirst()?.let {
                    realm.copyFromRealm(it) // so we can pass it between threads
                }
            }.asObservable()
        }
    }

    override fun getItems(query: String) = get<RealmItems>() { equalTo("query", query) }.filter { it.isUpToDate() }.map { it.asIItems() }
    override fun getUsers(query: String) = get<RealmUsers>() { equalTo("query", query) }.filter { it.isUpToDate() }.map { it.asIUsers() }
    override fun getRepos(query: String) = get<RealmRepos>() { equalTo("query", query) }.filter { it.isUpToDate() }.map { it.asIRepos() }

    // I do not actually use getUsers or getRepos now. Just getItems.



    override fun getUserDetails(login: String)
            = get<RealmUserDetails>() { equalTo("login", login) }.filter { it.isUpToDate() }
    override fun getRepoDetails(owner: String, repo: String)
            = get<RealmRepoDetails>() { equalTo("full_name", "$owner/$repo") }.filter { it.isUpToDate() }

}
