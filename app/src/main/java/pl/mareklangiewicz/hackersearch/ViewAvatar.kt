package pl.mareklangiewicz.hackersearch

import android.widget.ImageView
import com.squareup.picasso.Picasso

/** Created by Marek Langiewicz on 21.06.16. */
class ViewAvatar(private val iview: ImageView) : IViewImage {

    override var url = ""

        set(value) { // TODO SOMEDAY: handle invalid urls

            field = value

            if (value.isBlank())
                iview.setImageResource(R.drawable.hs_avatar)
            else
                Picasso.with(iview.context).load(url).into(iview)
        }
}