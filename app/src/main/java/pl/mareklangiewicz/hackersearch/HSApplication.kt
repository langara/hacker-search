package pl.mareklangiewicz.hackersearch

import android.app.Application
import io.realm.Realm

/** Created by Marek Langiewicz on 18.06.16. */
class HSApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
    companion object {
        val START_TIME = System.currentTimeMillis()
    }
}
