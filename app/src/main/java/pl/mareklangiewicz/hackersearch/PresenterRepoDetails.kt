package pl.mareklangiewicz.hackersearch

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import pl.mareklangiewicz.myloggers.MY_DEFAULT_ANDRO_LOGGER
import pl.mareklangiewicz.myutils.lsubscribe
import java.util.concurrent.TimeUnit


/** Created by Marek Langiewicz on 20.06.16. */
class PresenterRepoDetails : IPresenter<ViewRepoDetails> {

    var delay = 500L
    var minLength = 3

    private val log = MY_DEFAULT_ANDRO_LOGGER

    private val model = Model

    private var subInput: Disposable? = null
    private var subDetails: Disposable? = null

    override var view: ViewRepoDetails? = null

        set(v) {

            field = v

            subInput?.dispose()

            if(v === null)
                return

            working = working // sync ui..

            subInput = v.input.changes
                    .debounce(delay, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .filter { it.length >= minLength }
                    .map { it.split('/') }
                    .filter { it.size == 2}
                    .lsubscribe(log) {
                        getRepoDetails(it[0], it[1])
                    }
        }

    private var working: Boolean = false // true if we are running long operation (displays some moving progress bar)
        set(value) {
            field = value
            val v = view ?: return
            v.progress.indeterminate = value
        }

    private fun getRepoDetails(owner: String, name: String) {
        subDetails?.dispose()
        subDetails = model.getRepoDetails(owner, name)
                .doOnSubscribe { working = true }
                .doOnTerminate { working = false }
                .lsubscribe(log, logOnError = "[SNACK]Error: %s") {
                    showRepoDetails(it as IRepoDetails)
                }
    }

    private fun showRepoDetails(details: IRepoDetails?) {
        val v = view ?: return
        v.notes.notes = listOf(
                Note("Id", details?.id?.toString() ?: ""),
                Note("Name", details?.name ?: ""),
                Note("Full name", details?.full_name ?: ""),
                Note("Description", details?.description ?: ""),
                Note("Language", details?.language ?: ""),
                Note("GitHub page", details?.html_url ?: ""),
                Note("Home page", details?.homepage ?: ""),
                Note("Forks", details?.forks_count?.toString() ?: ""),
                Note("Stargazers", details?.stargazers_count?.toString() ?: ""),
                Note("Watchers", details?.watchers_count?.toString() ?: "")
        )
    }
}
