package pl.mareklangiewicz.hackersearch

import android.widget.ProgressBar

/** Created by Marek Langiewicz on 20.06.16. */
class ViewProgress(private val bar: ProgressBar) : IViewProgress {
    override var maximum: Int
        get() = bar.max
        set(value) { bar.max = value }
    override var current: Int
        get() = bar.progress
        set(value) { bar.progress = value }
    override var indeterminate: Boolean
        get() = bar.isIndeterminate
        set(value) { bar.isIndeterminate = value }
}