package pl.mareklangiewicz.hackersearch

import io.reactivex.Observable

/**
 * Created by Marek Langiewicz on 16.06.16.
 *
 * Interfaces defining overall architecture of the app.
 * It is a form of Model-View-Presenter pattern.
 */

interface IModel : IServiceItems, IServiceUsers, IServiceRepos, IClear

interface IView

interface IPresenter<V: IView> {

    /**
     * One can attach/detach (nullify) a view to a presenter many times in its lifecycle
     * Usually concrete presenters will do some additional stuff on attach/detach.
     * On android the presenter will usually live as long as fragment (with setRetainInstance(true))
     * And view will be attached on every onViewCreated, and detached on every onDestroyView.
     * This way presenter (with its fragment) survives device orientation changes etc...
     */
    var view: V?
}

/** simple view displaying some text - on android: usually just a thin wrapper on android.widget.TextView */
interface IViewText : IView {
    var text: String
}

/** IViewText that can emit text changes (usually implemented by android.widget.EditText) */
interface IViewInput : IViewText {
    val changes: Observable<String>
}

interface IViewImage : IView {
    var url: String // displays image available at specified url
}

/** A view that can display the IItem content, and can emit user click events */
interface IViewItem : IView {
    var item: IItem?
    val clicks: Observable<IViewItem>
}

/** A view displaying collection of IItems and emiting click events with particular IItems */
interface IViewItems : IView {
    var items: IItems?
    val clicks: Observable<IItem>
}

/** A view displaying simple note (a pair with: text label, and text content) */
interface IViewNote : IView {
    var note: Note?
}

/** A view displaying collection of notes. */
interface IViewNotes : IView {
    var notes: List<Note>?
}

/** A view that can present progress of some long running operation. */
interface IViewProgress {
    var maximum: Int // it is important inly when: indeterminate == false
    var current: Int // it is important only when: indeterminate == false
    var indeterminate: Boolean // true means it should display something indicating that it is working and we don't know how far we are
}

/** composes together all views on search screen */
interface IViewSearch : IView {
    val input: IViewInput
    val results: IViewItems
    val progress: IViewProgress
    val status: IViewText
}

/** composes together all views on user details screen */
interface IViewUserDetails : IView {
    val input: IViewInput
    val avatar: IViewImage
    val name: IViewText
    val description: IViewText
    val notes: IViewNotes
    val progress: IViewProgress
    val status: IViewText
}

/** composes together all views on repo details screen */
interface IViewRepoDetails : IView {
    val input: IViewInput
    val notes: IViewNotes
    val progress: IViewProgress
    val status: IViewText
}


/** common part of small data structures like IUser, IRepo */
interface IItem {
    val id: Long
    val title: String // "virtual" property (from IUser:login or IRepo:full_name)
    val type: String // also "virtual" - "user" for IUser, "repo" for IRepo
    val html_url: String?
}

interface IUser : IItem {
    override val title: String get() = login
    override val type: String get() = "user"
    val login: String
}

interface IUserDetails : IUser {
    val name: String?
    val avatar_url: String?
    val company: String?
    val location: String?
    val email: String?
    val public_repos: Long?
    val public_gists: Long?
    val followers: Long?
    val following: Long?
}

interface IRepo : IItem {
    override val title: String get() = full_name ?: name
    override val type: String get() = "repo"
    val name: String
    val full_name: String?
    val language: String?
}

interface IRepoDetails : IRepo {
    val description: String?
    val homepage: String?
    val forks_count: Long?
    val stargazers_count: Long?
    val watchers_count: Long?
}

interface IItems {
    val total_count: Long
    val incomplete_results: Boolean
    val items: List<IItem>
}

interface IUsers : IItems {
    override val items: List<IUser>
}

interface IRepos : IItems {
    override val items: List<IRepo>
}

data class Note(val label: String, val content: String)

/** A service that can provide list of items for particular query, and (optionally) save given list of items for given query */
interface IServiceItems {
    fun getItems(query: String): Observable<out IItems>
    fun putItems(query: String, items: IItems)
}

interface IServiceUsers {
    fun getUsers(query: String): Observable<out IUsers>
    fun putUsers(query: String, users: IUsers)
    fun getUserDetails(login: String): Observable<out IUserDetails>
    fun putUserDetails(user: IUserDetails)
}

interface IServiceRepos {
    fun getRepos(query: String): Observable<out IRepos>
    fun putRepos(query: String, repos: IRepos)
    fun getRepoDetails(owner: String, repo: String): Observable<out IRepoDetails>
    fun putRepoDetails(repo: IRepoDetails)
}

interface IClear { fun clear() }


