package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.RecyclerView
import android.widget.*

/** Created by Marek Langiewicz on 21.06.16. */
class ViewUserDetails(
        override val input: IViewInput,
        override val avatar: IViewImage,
        override val name: IViewText,
        override val description: IViewText,
        override val notes: IViewNotes,
        override val progress: IViewProgress,
        override val status: IViewText
) : IViewUserDetails {
    constructor(
            etinput: EditText,
            ivavatar: ImageView,
            tvname: TextView,
            tvdesc: TextView,
            rvnotes: RecyclerView,
            pbprogress: ProgressBar,
            tvstatus: TextView
    ) : this(
            ViewInput(etinput),
            ViewAvatar(ivavatar),
            ViewText(tvname),
            ViewText(tvdesc),
            ViewNotes(rvnotes),
            ViewProgress(pbprogress),
            ViewText(tvstatus)
    )
}
