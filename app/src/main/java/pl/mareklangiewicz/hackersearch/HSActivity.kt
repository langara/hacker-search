package pl.mareklangiewicz.hackersearch

import pl.mareklangiewicz.myactivities.*
import pl.mareklangiewicz.myutils.*
import android.os.Bundle

/**
 * Created by Marek Langiewicz on 19.06.16.
 * Main (and only) activity.
 * I use MyActivity base class (from MyIntent library) to get outer UI template and other features.
 * (It offers a drawer with header and main menu, fragment switching commands, automatic layout change for wide devices,...)
 */
class HSActivity : MyActivity() {

    var query: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navigation = gnav!!
        navigation.headerId = R.layout.hs_header
        navigation.menuId = R.menu.hs_menu
        navigation.items {
            when(it) {
                R.id.hs_clear_data -> { Model.clear(); log.i("[SNACK]All cached data deleted.") }
            }
        }
        if (savedInstanceState == null) navigation.setCheckedItem(R.id.hs_section_search, true)
    }
}