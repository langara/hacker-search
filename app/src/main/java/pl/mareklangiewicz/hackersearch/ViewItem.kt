package pl.mareklangiewicz.hackersearch

import android.support.v7.widget.CardView
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.hs_item.view.*
import io.reactivex.Observable

/** Created by Marek Langiewicz on 21.06.16. */
class ViewItem(private val cview: CardView) : IViewItem {

    override var item: IItem? = null
        set(value) {
            field = value
            if(value === null) {
                cview.hs_item_head.text = ""
                cview.hs_item_title.text = ""
            }
            else {
                cview.hs_item_head.text = "  ${value.type.toUpperCase()}  \n${value.id.toString().padStart(9, '0')}"
                cview.hs_item_title.text = "${value.title}\n${value.html_url ?: ""}"
            }
        }

    override val clicks: Observable<IViewItem> = cview.clicks().map { this }
}
