package pl.mareklangiewicz.hackersearch

import org.junit.Test
import pl.mareklangiewicz.myutils.MySystemLogger
import pl.mareklangiewicz.myutils.lsubscribe


class GitHubTests {

    private val log = MySystemLogger()

    // tests below are not really proper unit tests. just some simple code for manual testing/logging/debugging

    private val service = GitHub
//    private val service = GitHubCache // can not test GitHubCache here (needs android). check: androidTest/.../GitHubCacheTests.kt
//    private val service = Model // can not test Model facade here (needs android). check: androidTest/.../GitHubCacheTests.kt

    @Test fun getUserDetails1() { service.getUserDetails("langara").lsubscribe(log) }
    @Test fun getUserDetails2() { service.getUserDetails("JakeWharton").lsubscribe(log) }
    @Test fun getRepoDetails1() { service.getRepoDetails("langara", "MyIntent").lsubscribe(log) }
    @Test fun getRepoDetails2() { service.getRepoDetails("JakeWharton", "RxBinding").lsubscribe(log) }
    @Test fun getUsers1() { service.getUsers("langara").lsubscribe(log) }
    @Test fun getUsers2() { service.getUsers("JakeWharton").lsubscribe(log) }
    @Test fun getRepositories1() { service.getRepos("langara").lsubscribe(log) }
    @Test fun getRepositories2() { service.getRepos("JakeWharton").lsubscribe(log) }
}