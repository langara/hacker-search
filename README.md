# Hacker Search

Simple GitHub searching app written in Kotlin with RxJava.

#### Features

* Automatic search for users and repositories
* Fetching details of any selected/entered user or repository.
* Offline browsing (once visited users/repos data is saved in local data base)
* A form of MVP architecture
* Material design (kind of..)
* Cool layout for wide screens (720dp or more) :-)
* Uses the the https://developer.github.com/v3 API

#### Some interesting libraries used

* Retrofit 2
* RxJava
* RxAndroid
* RxBinding
* RxRelay
* Realm
* Picasso
* Paperwork
* Material Dialogs
* My Intent


#### Implementation notes

* Base interfaces defining the app architecture are in: Architecture.kt

  - I keep implementation inheritance hierarchy flat, so classes depends only on interfaces
    (not particular implementations), so different parts of app can be reimplemented,
    mocked for tests, etc..

* I've used a Realm.io database for cache implementation, but the overall architecture
  is NOT realm style.

  - Especially I do not keep live (managed) realm objects around - I immediately
    copy it to unmanaged objects so I can move it between threads (RxJava style)

  - This approach allows me to encapsulate whole Realm.io related details in one class (GitHubCache)
    and potentially switch it to other database implementation in the future. And to test
    different parts of the app in separation easily.

  - One disadvantage is: I can not use special features of Realm.io like auto updating RealmObjects
    and RealmResults; special base adapter for RecyclerView: RealmRecyclerViewAdapter, etc..

* Some additional features (like logger screen, changed app name) are enabled only
  in debug build type, so user will not see it.

* I've used my own library: MyIntent to get some ready to use outer layout, and some other
  features (like automatic fragment switching, logging). It works well, and I know it well,
  so it is good enough for such small app like this one, but:

  - I wouldn't use it in some bigger commercial project because it isn't really prepared for
    wide usage by others (tested only by me, not many comments, a lot of experimental stuff,
    unfinished code, not widely accepted coding convensions, etc..)

  - I guess in bigger projects it is better to stick with widely used and tested libraries - like
    those from Square :)


